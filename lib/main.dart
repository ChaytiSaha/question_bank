import 'package:flutter/material.dart';
import 'package:question_bank/quizBank.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Quiz Bank',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: QuizPageState());
  }
}

class QuizPageState extends StatefulWidget {
  @override
  _QuizPageStateState createState() => _QuizPageStateState();
}

class _QuizPageStateState extends State<QuizPageState> {

  QuizBrain quizBrain = QuizBrain();

  List<Icon> scoreKeeper = [];
  void checkAnswer(bool userPickedAnswer){
    setState(() {
      if(quizBrain.isFinished() == true){
        Alert(
          context: context,
          title: "শেষ",
          desc: "আপনার কুইজ সফলভাবে সম্পন্ন হয়েছে",
        ).show();

        quizBrain.reset();
        scoreKeeper=[];
      }
      else {
        if (quizBrain.getQuestionAnswer() == userPickedAnswer) {
          scoreKeeper.add(Icon(Icons.check, color: Colors.green));
        }
        else {
          scoreKeeper.add(Icon(Icons.close, color: Colors.red));
        }
        quizBrain.nextQuestion();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Expanded(
          flex: 5,
          child: Center(
            child: Padding(
              padding: EdgeInsets.all(5.10),
              child: Text(
                quizBrain.getQuestionText(),
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 25.0,
                    color: Colors.white,
                    decoration: TextDecoration.none //delete underline
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: Container(
            color: Colors.green,
            child: Padding(
              padding: EdgeInsets.all(5.10),
              child: TextButton(
              child: Text(
                'সত্য',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 25.0,
                    color: Colors.white
                ),
              ),
                onPressed: (){
                  setState(() {
                    checkAnswer(true);
                  });
                },
              ),
            ),
          ),
        ),

        SizedBox(
          height: 8,
        ),

        Expanded(
          child: Container(
            color: Colors.red,
            child: Padding(
              padding: EdgeInsets.all(5.10),
              child: TextButton(
                child: Text(
                  'মিত্থ্যা',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 25.0,
                      color: Colors.white
                  ),
                ),
                onPressed: (){
                  setState(() {
                    checkAnswer(false);
                  });
                },
              ),
            ),
          ),
        ),

        SizedBox(
          height: 8,
        ),

        Row(
          children: scoreKeeper,
        )

      ],
    );
  }
}
