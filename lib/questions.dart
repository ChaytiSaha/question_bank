class Questions{
  late String questionText;
  late bool questionAnswer;

  Questions({required String questionText, required bool questionAnswer}){
    this.questionText = questionText;
    this.questionAnswer = questionAnswer;
  }
}